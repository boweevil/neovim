THIS_DIR = $(shell cd -- "$( dirname -- "${BASH_SORCE[0]}" )" &> /dev/null && pwd )
PARENT_DIR = $(shell dirname $(THIS_DIR))

.PHONY: test_local
test_local:
	XDG_CONFIG_HOME=$(PARENT_DIR) nvim
