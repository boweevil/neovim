" ----------------------------------------------------------------------------
" TOGGLE LIST CHARACTERS:
" ----------------------------------------------------------------------------
"  List all special characters.
function! ListAll()
  set listchars=tab:│\ ,trail:·,precedes:«,extends:»,eol:¶
endfunction

" Return to only listing trailing blank space characters.
function! ListTrail()
  set listchars=tab:│\ ,trail:·
endfunction

function! ToggleListCharacters()
  if &listchars == 'tab:│ ,trail:·'
    call ListAll()
  else
    call ListTrail()
  endif
endfunction

nmap <F8> :call ToggleListCharacters()<cr>

" ----------------------------------------------------------------------------
" TOGGLE COLOR COLUMN:
" ----------------------------------------------------------------------------
function! ColorColumnEightyEight()
  " Set the margin to 88.  This colors column 89.
  set colorcolumn=89
  " highlight ColorColumn ctermbg=1 guibg=Red
endfunction

function! ToggleColorColumn()
  " Cycle through column 80, 120, and disabled.
  if &colorcolumn == 0
    call ColorColumnEightyEight()
  elseif &colorcolumn ==# '89'
    set colorcolumn=0
  endif
endfunction

nmap <F7> :call ToggleColorColumn()<cr>

" ----------------------------------------------------------------------------
" TOGGLE CURSOR LINE AND COLUMN:
" ----------------------------------------------------------------------------
function! ToggleCursorRowCol()
  if &cursorline == 0 && &cursorcolumn == 0
    set cursorline
    set cursorcolumn
  elseif &cursorline == 1 && &cursorcolumn == 1
    set nocursorcolumn
  elseif &cursorline == 1 && &cursorcolumn == 0
    set nocursorline
  endif
endfunction

nmap <F6> :call ToggleCursorRowCol()<cr>

" ----------------------------------------------------------------------------
" FILL REST OF LINE WITH CHARACTERS:
" ----------------------------------------------------------------------------
function! FillLine( str )
  " Set tw to the desired total length
  let l:tw = &textwidth
  if l:tw==0 | let l:tw = 79 | endif
  " Strip trailing spaces first
  .s/[[:space:]]*$//
  " Calculate total number of 'str's to insert
  let l:reps = (l:tw - col('$')) / len(a:str)
  " Insert them, if there's room, removing trailing spaces (though forcing
  " there to be one)
  if l:reps > 0
    .s/$/\=(' '.repeat(a:str, reps))/
  endif
endfunction

nmap <leader>= :call FillLine( '=' )<cr>
nmap <leader>- :call FillLine( '-' )<cr>
nmap <leader># :call FillLine( '#' )<cr>

" ----------------------------------------------------------------------------
" REMOVE TRAILING BLANK SPACE:
" ----------------------------------------------------------------------------
function! StripTrailingWhitespaces()
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    %s/\s\+$//e
    " Clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
endfunction

nnoremap <silent> <leader>rtw :call StripTrailingWhitespaces()<cr>
