let g:airline_detect_modified = 1
let g:airline_detect_paste = 1
let g:airline_detect_crypt = 1
let g:airline_detect_spell = 1

let g:airline_inactive_collapse = 1
let g:airline_powerline_fonts = 1
let g:airline_symbols_ascii = 0
let g:airline_save_space = 1

" Tabline settings
let g:airline#extensions#tabline#alt_sep = 0
let g:airline#extensions#tabline#buf_label_first = 1
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#tabline#ctrlspace_show_tab_nr = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#show_splits = 1
let g:airline#extensions#tabline#show_tab_count = 2
let g:airline#extensions#tabline#show_tab_nr = 0
let g:airline#extensions#tabline#show_tab_type = 1
let g:airline#extensions#tabline#show_tabs = 1
let g:airline#extensions#tabline#switch_buffers_and_tabs = 0

" Coc
let g:airline#extensions#coc#enabled = 1
let airline#extensions#coc#error_symbol = 'Error:'
let airline#extensions#coc#warning_symbol = 'Warning:'
let airline#extensions#coc#stl_format_err = '%E{[%e(#%fe)]}'
let airline#extensions#coc#stl_format_warn = '%W{[%w(#%fw)]}'


let g:airline_skip_empty_sections = 0

" Save space
if g:airline_save_space
let g:airline_mode_map = {
  \ '__' : '-',
  \ 'n'  : 'N',
  \ 'i'  : 'I',
  \ 'R'  : 'R',
  \ 'c'  : 'C',
  \ 'v'  : 'Vi',
  \ 'V'  : 'VL',
  \ '' : 'VB',
  \ 's'  : 'S',
  \ 'S'  : 'SL',
  \ '' : 'SB',
  \ 't'  : 'T',
  \ }
endif

function! GetColorScheme()
  try
    echo g:colors_name
  catch /^Vim:E121/
    echo 'default'
  endtry
endfunction

set laststatus=2
set noshowmode


" There is a pause when leaving insert mode:
" You need to set ttimeoutlen; 10 is a good number to start with.
" Many places say to edit timeoutlen, but that could conflict with other plugins.
set ttimeoutlen=10


" let g:airline_theme='gruvbox'
