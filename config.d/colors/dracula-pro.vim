let g:dracula_bold = 1
let g:dracula_italic = 1
let g:dracula_underline = 1
let g:dracula_undercurl = 1
let g:dracula_invers = 1
let g:dracula_colorterm = 0

if (has("termguicolors"))
  set termguicolors
endif

set t_ut=
set t_Co=256
let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
let &t_Cs = "\e[4:3m"
let &t_Ce = "\e[4:0m"

try
  set background=dark
  colorscheme dracula_pro
  let g:airline_theme='dracula_pro'
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
endtry
