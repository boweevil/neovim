let g:gruvbox_contrast_light='medium'
let g:gruvbox_contrast_dark='medium'
let g:gruvbox_color_column='bg0'
let g:gruvbox_number_column='bg0'
let g:gruvbox_sign_column='bg0'
let g:gruvbox_vert_split='bg0'
let g:gruvbox_improved_strings=0  " This looks horrible in terminal
let g:gruvbox_improved_warnings=0
let g:gruvbox_invert_indent_guides=0
let g:gruvbox_invert_selection=0
let g:gruvbox_invert_signs=0
let g:gruvbox_invert_tabline=0
let g:gruvbox_bold=1
let g:gruvbox_italic=1
let g:gruvbox_italicize_comments=1
let g:gruvbox_italicize_strings=1
let g:gruvbox_underline=1
let g:gruvbox_undercurl=1
let g:gruvbox_termcolors=256

if (has("termguicolors"))
  set termguicolors
endif

set t_ut=
set t_Co=256
let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
let &t_Cs = "\e[4:3m"
let &t_Ce = "\e[4:0m"

try
  set background=dark
  colorscheme gruvbox
  let g:airline_theme='gruvbox'
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
endtry
