let g:gruvbox_material_palette = 'material'
let g:gruvbox_material_background = 'hard'
let g:gruvbox_material_enable_bold = 1
let g:gruvbox_material_enable_italic = 1
let g:gruvbox_material_disable_italic_comment = 0
let g:gruvbox_material_cursor = 'auto'
let g:gruvbox_material_transparent_background = 0
let g:gruvbox_material_visual = 'reverse'
let g:gruvbox_material_menu_selection_background = 'green'
let g:gruvbox_material_sign_column_background = 'none'
let g:gruvbox_material_spell_foreground = 'colored'
let g:gruvbox_material_ui_contrast = 'low'
let g:gruvbox_material_show_eob = 0
let g:gruvbox_material_diagnostic_text_highlight = 1
let g:gruvbox_material_diagnostic_line_highlight = 0
let g:gruvbox_material_diagnostic_virtual_text = 'grey'
let g:gruvbox_material_current_word = 'grey background'
let g:gruvbox_material_statusline_style = 'default'
let g:gruvbox_material_lightline_disable_bold = 0
let g:gruvbox_material_better_performance = 1

if (has("termguicolors"))
  set termguicolors
endif

set t_ut=
set t_Co=256
let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
let &t_Cs = "\e[4:3m"
let &t_Ce = "\e[4:0m"

try
  set background=dark
  colorscheme gruvbox-material
  let g:airline_theme='gruvbox_material'
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
endtry
