if (has("termguicolors"))
  set termguicolors
endif

set t_ut=
set t_Co=256
let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
let &t_Cs = "\e[4:3m"
let &t_Ce = "\e[4:0m"

function! s:base16_customize() abort
  call Base16hi("MatchParen", g:base16_gui0E, g:base16_gui00, g:base16_cterm0E, g:base16_cterm00, "bold", "")

  " Diff highlighting
  call Base16hi("DiffAdd",      g:base16_gui00, g:base16_gui0B,  g:base16_cterm00, g:base16_cterm0B, "", "")
  call Base16hi("DiffChange",   g:base16_gui00, g:base16_gui0C,  g:base16_cterm00, g:base16_cterm0C, "", "")
  call Base16hi("DiffDelete",   g:base16_gui00, g:base16_gui08,  g:base16_cterm00, g:base16_cterm08, "", "")
  call Base16hi("DiffText",     g:base16_gui00, g:base16_gui0A,  g:base16_cterm00, g:base16_cterm0A, "", "")
  call Base16hi("DiffAdded",    g:base16_gui00, g:base16_gui0B,  g:base16_cterm00, g:base16_cterm0B, "", "")
  call Base16hi("DiffFile",     g:base16_gui00, g:base16_gui08,  g:base16_cterm00, g:base16_cterm08, "", "")
  call Base16hi("DiffNewFile",  g:base16_gui00, g:base16_gui0B,  g:base16_cterm00, g:base16_cterm0B, "", "")
  call Base16hi("DiffLine",     g:base16_gui00, g:base16_gui0A,  g:base16_cterm00, g:base16_cterm0A, "", "")
  call Base16hi("DiffRemoved",  g:base16_gui00, g:base16_gui08,  g:base16_cterm00, g:base16_cterm08, "", "")
endfunction

try
  let base16colorspace=256
  let g:base16_shell_path="~/.config/base16-shell/scripts/"
  source ~/.vimrc_background
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
endtry

augroup on_change_colorschema
  autocmd!
  autocmd ColorScheme * call s:base16_customize()
augroup END
