" Python 2 mode
let g:python_version_2 = 0

" Python 2 mode (buffer local)
let b:python_version_2 = 0

" Enable all highlight options above, except for previously set.
let g:python_highlight_all = 0

" Highlight builtin objects, types, and functions
let g:python_highlight_builtins = 1

" Highlight builtin objects only
let g:python_highlight_builtin_objs = 0

" Highlight builtin types only
let g:python_highlight_builtin_types = 0

" Highlight builtin functions only
let g:python_highlight_builtin_funcs = 0

" Highlight builtin functions when used as kwarg
let g:python_highlight_builtin_funcs_kwarg = 0

" Highlight standard exceptions
let g:python_highlight_exceptions = 0

" Highlight % string formatting
let g:python_highlight_string_formatting = 0

" Highlight syntax of str.format syntax
let g:python_highlight_string_format = 1

" Highlight syntax of string.Template
let g:python_highlight_string_templates = 0

" Highlight indentation errors
let g:python_highlight_indent_errors = 1

" Highlight trailing spaces
let g:python_highlight_space_errors = 1

" Highlight doc-tests
let g:python_highlight_doctests = 0

" Highlight functions calls
let g:python_highlight_func_calls = 0

" Highlight class variables self, cls, and mcs
let g:python_highlight_class_vars = 1

" Highlight all operators
let g:python_highlight_operators = 1

" Highlight shebang and coding headers as comments
let g:python_highlight_file_headers_as_comments = 1

" Disable for slow machines
let g:python_slow_sync = 0
