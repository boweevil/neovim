let g:ctrlp_show_hidden = 1
let g:ctrlp_working_path_mode = 0

nnoremap <F3> :CtrlPBuffer<cr>
