" For example, to map it to <F8>:
" autocmd FileType python noremap <buffer> <F8> :call Autopep8()<CR>

" Do not fix these errors/warnings (default: E226,E24,W6)
" let g:autopep8_ignore="E501,W293"
" Fix only these errors/warnings (e.g. E4,W)
" let g:autopep8_select="E133,E124,E123"

" Maximum number of additional pep8 passes (default: 100)
let g:autopep8_pep8_passes=100

" Set maximum allowed line length (default: 79)
let g:autopep8_max_line_length=79

" # add more aggressive options (--aggressive --aggressive) 1 or 2
let g:autopep8_aggressive=2

" Number of spaces per indent level (default: 4)
let g:autopep8_indent_size=4

"Disable show diff window
let g:autopep8_disable_show_diff=1

" Chose diff window type. (default: horizontal)
let g:autopep8_diff_type='horizontal'

" Automatically format every time saving a file.
let g:autopep8_on_save = 1
