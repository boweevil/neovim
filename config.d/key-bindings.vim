" Edit or source config
nnoremap <leader>ve :edit ~/.config/nvim/init.vim<cr>
nnoremap <leader>vc :edit ~/.config/nvim/coc-settings.json<cr>
nnoremap <leader>vr :source ~/.config/nvim/init.vim <bar> AirlineRefresh<cr>

nnoremap <leader>pi :PlugInstall<cr>
nnoremap <leader>pu :PlugUpdate<cr>

if has('macunix')
    nnoremap <leader>x :!open %<cr><cr>
elseif has('unix')
    nnoremap <leader>x :!xdg-open %<cr><cr>
endif

" Search for selected.
vnoremap // y/<c-r>"<cr>

" Toggle Spell Check with \s.
nnoremap <leader>s :set spell!<cr>

" Switch between buffers
nnoremap <leader>pb :bprevious<cr>
nnoremap <leader>nb :bnext<cr>

" Switch between tabs
nnoremap <leader>pt :tabprevious<cr>
nnoremap <leader>nt :tabnext<cr>

" Run make test
nnoremap <leader>tb :!clear;make test<cr>

" Run make coverage
nnoremap <leader>c :!clear;make coverage<cr>

" Pretty print json
nnoremap <leader>pp :%!python -m json.tool<cr>

" Toggle relative numbers
nnoremap <leader>nn :set relativenumber!<cr>

" Easy insertion of a trailing ; or ,
nnoremap ;; A;<Esc>
nnoremap ,, A,<Esc>
" from Insert mode
imap ;; <Esc>A;<Esc><c-o>i
imap ,, <Esc>A,<Esc><c-o>i
