let g:loaded_python_provider = 0

if filereadable('~/.pyenv/shims/python2')
  let g:python_host_prog = '~/.pyenv/shims/python2'
endif

if filereadable('~/.pyenv/shims/python3')
  let g:python3_host_prog = '~/.pyenv/shims/python3'
endif

" Highlight docstrings as comments
autocmd User PlugLoaded syn region Comment start=/"""/ end=/"""/
