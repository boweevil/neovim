" WhichKey is a lua plugin for Neovim 0.5 that displays a popup with possible key bindings of the command you started typing.
lua << EOF
    require("which-key").setup {}
EOF
