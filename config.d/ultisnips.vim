" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger='<c-j>'
let g:UltiSnipsListSnippets='<c-tab>'
let g:UltiSnipsJumpForwardTrigger='<c-j>'
let g:UltiSnipsJumpBackwardTrigger='<c-k>'

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit='vertical'

" Tell UltiSnips where the custom snippets go.
let g:UltiSnipsSnippetsDir= $HOME.'/.UltiSnips/'
let g:UltiSnipsSnippetDirectories=['~/.UltiSnips']
