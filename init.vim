"=============================================================================
" PLUGINS:
"=============================================================================
let s:nvim_dir = '~/.config/nvim/'
let s:vimplug_path = s:nvim_dir . '/autoload/plug.vim'
if empty(glob(s:vimplug_path))
  silent execute '!curl -fLo '.s:vimplug_path.' --create-dirs '.
    \'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | close | source $MYVIMRC | AirlineRefresh
endif

call plug#begin(s:nvim_dir . 'packages')

" COLORSCHEME PLUGINS: -------------------------------------------------------
" Plug 'lifepillar/vim-solarized8'
Plug 'catppuccin/nvim'

" IDE PLUGINS: ---------------------------------------------------------------
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'kyazdani42/nvim-tree.lua'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'vim-ctrlspace/vim-ctrlspace'
Plug 'tpope/vim-fugitive'
Plug 'nvim-lua/plenary.nvim'
Plug 'sindrets/diffview.nvim'
Plug 'voldikss/vim-floaterm'
Plug 'junegunn/vim-peekaboo'
Plug 'unblevable/quick-scope'
Plug 'jessarcher/vim-sayonara', { 'on': 'Sayonara' }
Plug 'terryma/vim-smooth-scroll'
Plug 'vim-test/vim-test'
Plug 'folke/which-key.nvim'
Plug 'gpanders/editorconfig.nvim'

" FORMATTING PLUGINS: --------------------------------------------------------
Plug 'SirVer/ultisnips'
  Plug 'honza/vim-snippets'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
  Plug 'tpope/vim-repeat'
Plug 'tomtom/tcomment_vim'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'wellle/targets.vim'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'nelstrom/vim-visual-star-search'

" SYNTAX PLUGINS: ------------------------------------------------------------
" polyglot_disabled must be set before vim-polyglot is loaded
let g:polyglot_disabled = ['dockerfile']
Plug 'sheerun/vim-polyglot'
Plug 'cappyzawa/starlark.vim'
Plug 'towolf/vim-helm'
Plug 'imsnif/kdl.vim'

" VIMSCRIPT PLUGINS: -------------------------------------------------------
Plug 'lifepillar/vim-colortemplate'


" PYTHON PLUGINS: ------------------------------------------------------------
Plug 'vim-python/python-syntax', { 'for': ['python'] }
Plug 'psf/black', { 'tag': '*' }
Plug 'nvie/vim-flake8', { 'for': ['python'] }

" GOLANG PLUGINS: ------------------------------------------------------------
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" RUST PLUGINS: --------------------------------------------------------------
Plug 'rust-lang/rust.vim'
Plug 'neovim/nvim-lspconfig'
Plug 'simrat39/rust-tools.nvim'
" -- Debugging
Plug 'nvim-lua/plenary.nvim'
Plug 'mfussenegger/nvim-dap'

" MARKDOWN PLUGINS: ----------------------------------------------------------
Plug 'iamcco/markdown-preview.nvim'
Plug 'gabrielelana/vim-markdown'

" ANSIBLE PLUGINS: -----------------------------------------------------------
Plug 'pearofducks/ansible-vim'

" TERRAFORM PLUGINS: ---------------------------------------------------------
Plug 'hashivim/vim-terraform'

" NUSHELL PLUGINS: -----------------------------------------------------------
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'LhKipp/nvim-nu', {'do': ':TSInstall nu'}
Plug 'jose-elias-alvarez/null-ls.nvim'
Plug 'nvim-lua/plenary.nvim'

" OTHER PLUGINS: -------------------------------------------------------------
Plug 'gerw/vim-HiLinkTrace'
Plug 'vim-scripts/VisIncr'
Plug 'ekalinin/Dockerfile.vim'

call plug#end()

"=============================================================================
" CONFIGURATION:
"=============================================================================

" COLORSCHEME PLUGINS: -------------------------------------------------------
execute 'source' s:nvim_dir . 'config.d/colors/vim-solarized8.vim'

" IDE PLUGINS: ---------------------------------------------------------------
execute 'source' s:nvim_dir . 'config.d/coc.vim'
execute 'source' s:nvim_dir . 'config.d/nvim-tree.lua'
execute 'source' s:nvim_dir . 'config.d/vim-airline.vim'
execute 'source' s:nvim_dir . 'config.d/vim-gitgutter.vim'
execute 'source' s:nvim_dir . 'config.d/ctrlp.vim'
execute 'source' s:nvim_dir . 'config.d/ctrlspace.vim'
execute 'source' s:nvim_dir . 'config.d/vim-fugitive.vim'
execute 'source' s:nvim_dir . 'config.d/floaterm.vim'
execute 'source' s:nvim_dir . 'config.d/quickscope.vim'
execute 'source' s:nvim_dir . 'config.d/sayonara.vim'
execute 'source' s:nvim_dir . 'config.d/smooth-scroll.vim'
execute 'source' s:nvim_dir . 'config.d/vim-test.vim'
execute 'source' s:nvim_dir . 'config.d/which-key.vim'

" FORMATTING PLUGINS: --------------------------------------------------------
execute 'source' s:nvim_dir . 'config.d/ultisnips.vim'
execute 'source' s:nvim_dir . 'config.d/splitjoin.vim'

" VIMSCRIPT PLUGINS: -------------------------------------------------------
execute 'source' s:nvim_dir . 'config.d/vim-colortemplate.vim'

" PYTHON PLUGINS: ------------------------------------------------------------
execute 'source' s:nvim_dir . 'config.d/python.vim'
execute 'source' s:nvim_dir . 'config.d/python-syntax.vim'
execute 'source' s:nvim_dir . 'config.d/black.vim'

" GOLANG PLUGINS: ------------------------------------------------------------
execute 'source' s:nvim_dir . 'config.d/golang.vim'
execute 'source' s:nvim_dir . 'config.d/vim-go.vim'

" RUST PLUGINS: --------------------------------------------------------------
execute 'source' s:nvim_dir . 'config.d/rust-tools.lua'

" MARKDOWN PLUGINS: ----------------------------------------------------------
execute 'source' s:nvim_dir . 'config.d/markdown-preview.vim'

" NUSHELL PLUGINS ------------------------------------------------------------
execute 'source' s:nvim_dir . 'config.d/nvim-nu.lua'
execute 'source' s:nvim_dir . 'config.d/treesitter.lua'

" GENERAL CONFIGURATION: -----------------------------------------------------
execute 'source' s:nvim_dir . 'config.d/general-configuration.vim'
execute 'source' s:nvim_dir . 'config.d/key-bindings.vim'
execute 'source' s:nvim_dir . 'config.d/functions.vim'
execute 'source' s:nvim_dir . 'config.d/file-types.vim'
