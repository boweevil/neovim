#!/usr/bin/env python3
"""
Setup NeoVim
"""

import os
from pathlib import Path
import shutil
import subprocess
import urllib.request

APP = "nvim"
APP_NAME = "neovim"
HOME = str(Path.home())
APP_DIR = f"{HOME}/.config/nvim"
APP_PLUG_INSTALL_PREFIX = f"{APP_DIR}/autoload"
PACKAGES_DIR = f"{APP_DIR}/packages"

# Custom tasks ----------------------------------------------------------------
print(f"Setting up {APP_NAME.upper()}...")

# Install vim-plug
if not os.path.isdir(APP_PLUG_INSTALL_PREFIX):
    os.mkdir(APP_PLUG_INSTALL_PREFIX)

if not os.path.isfile(f"{APP_PLUG_INSTALL_PREFIX}/plug.vim"):
    urllib.request.urlretrieve(
        "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim",
        f"{APP_PLUG_INSTALL_PREFIX}/plug.vim",
    )

if not os.path.isdir(PACKAGES_DIR):
    print(f"Installing {APP_NAME.upper()} plug-ins")
    subprocess.run([APP, "+PlugInstall", "+qall"], check=True)

    if not os.path.isdir(PACKAGES_DIR):
        print(f"Failed to upgrade and install {APP_NAME.upper()} plugins.")
