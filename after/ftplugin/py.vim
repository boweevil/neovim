setlocal autoindent
setlocal encoding=utf-8
setlocal expandtab
setlocal fileformat=unix
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal tabstop=4
setlocal textwidth=79

" Enable folding
setlocal foldmethod=indent
setlocal foldlevel=99
